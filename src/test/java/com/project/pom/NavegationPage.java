package com.project.pom;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class NavegationPage extends BaseProject {
	private WebDriver driver;
	NavegationPage navegationPage;
	
	//Locators
	By navsearch = By.name("as_word");
	By homePageLocator = By.id("nav-skip-to-main-content");
	By searchBtn = By.xpath("/html/body/header/div/form/button");
	
	By page01 = By.xpath("//*[@id=\"root-app\"]/div/div/section/ol/li[1]/div/div/a/div/div[3]/h2");
	By page02 = By.xpath("//*[@id=\"root-app\"]/div/div/section/ol/li[2]/div/div/a/div/div[3]/h2");
	By page03 = By.xpath("//*[@id=\"root-app\"]/div/div/section/ol/li[3]/div/div/a/div/div[3]/h2");
	By page04 = By.xpath("//*[@id=\"root-app\"]/div/div/section/ol/li[4]/div/div/a/div/div[3]/h2");
	By page05 = By.xpath("//*[@id=\"root-app\"]/div/div/section/ol/li[5]/div/div/a/div/div[3]/h2");
	
	By url01 = By.xpath("//*[@id=\"root-app\"]/div/div/section/ol/li[1]/div/div/a");
	By url02 = By.xpath("//*[@id=\"root-app\"]/div/div/section/ol/li[2]/div/div/a");
	By url03 = By.xpath("//*[@id=\"root-app\"]/div/div/section/ol/li[3]/div/div/a");
	By url04 = By.xpath("//*[@id=\"root-app\"]/div/div/section/ol/li[4]/div/div/a");
	By url05 = By.xpath("//*[@id=\"root-app\"]/div/div/section/ol/li[5]/div/div/a");
	

	public NavegationPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	//Metodo para navegar en "Mercadolibre" y realiar la busqueda
	public void Navegation() throws InterruptedException {
		click(navsearch);
		Thread.sleep(2000);
		if(isDisplayed(navsearch)) {
			type("autos", navsearch);
			click(searchBtn);
		}
		else {
			System.out.println("Note: Your search is not found");		
		}	
	}
	
	public boolean isHomePageDisplayed() {
		return isDisplayed(homePageLocator);
		
	}
	
	//Para obtener los datos de toda la p�gina se debe ocupar un for, en este caso se obtuvieon los datos de a uno
	public void Data() throws InterruptedException, Exception{	
		WebElement TxtBoxContent = driver.findElement(page01);
		String txt = TxtBoxContent.getText();
		
		WebElement TxtBoxContent2 = driver.findElement(page02);
		String txt2 = TxtBoxContent2.getText();	
		
		WebElement TxtBoxContent3 = driver.findElement(page03);
		String txt3 = TxtBoxContent3.getText();
		
		WebElement TxtBoxContent4 = driver.findElement(page04);
		String txt4 = TxtBoxContent4.getText();
		
		WebElement TxtBoxContent5 = driver.findElement(page05);
		String txt5 = TxtBoxContent5.getText();
		
		WebElement TxtBoxUrl = driver.findElement(url01);
		String txt6 = TxtBoxUrl.getAttribute("href");
		
		WebElement TxtBoxUrl2 = driver.findElement(url02);
		String txt7 = TxtBoxUrl2.getAttribute("href");
		
		WebElement TxtBoxUrl3 = driver.findElement(url03);
		String txt8 = TxtBoxUrl3.getAttribute("href");
		
		WebElement TxtBoxUrl4 = driver.findElement(url04);
		String txt9 = TxtBoxUrl4.getAttribute("href");
		
		WebElement TxtBoxUrl5 = driver.findElement(url05);
		String txt10 = TxtBoxUrl5.getAttribute("href");
		
		
		//Archivo txt de resultados
		PrintWriter out = null;
		try {
		    out = new PrintWriter(new FileWriter("./src/test/resources/resultValues.txt"));
		    } catch (IOException e) {
		            e.printStackTrace();
		    }
		out.println("Nombre de los titulos: \n"+
				txt+"\n"+ 
		        txt2+"\n"+
		        txt3+"\n"+
		        txt4+"\n"+
		        txt5+"\n"+
		        
		        "Link de Autos: \n"+
				txt6+"\n"+ 
		        txt7+"\n"+
		        txt8+"\n"+
		        txt9+"\n"+
		        txt10+"\n");
		out.close();
	}

}
