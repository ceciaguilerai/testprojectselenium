package com.project.pom;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class NavegationTest {
	
	private WebDriver driver;
	NavegationPage navegationPage;

	@Before
	public void setUp() throws Exception {
		navegationPage = new NavegationPage(driver);
		driver = navegationPage.chromeDriverConnection();
		driver.manage().window().maximize();
		navegationPage.visit("https://www.mercadolibre.cl/");
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

	@Test
	public void test() throws InterruptedException, Exception {
		navegationPage.Navegation();
		Thread.sleep(2000);
		assertTrue(navegationPage.isHomePageDisplayed());
		navegationPage.Data();	
		
		

		
	}
	
	

}
